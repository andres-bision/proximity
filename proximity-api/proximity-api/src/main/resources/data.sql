DROP TABLE IF EXISTS machine;
CREATE TABLE machine (
 id INT AUTO_INCREMENT  PRIMARY KEY,
 name VARCHAR(250) NOT NULL,
 model VARCHAR(250) NOT NULL
);

INSERT INTO machine (name,model) VALUES ('Maquina La Fonte', 'XYZ1');
INSERT INTO machine (name,model) VALUES ('Maquina La Muni', 'XYZ2');



/* ---------------------------------------------------------- */

DROP TABLE IF EXISTS item;
CREATE TABLE item (
 id INT AUTO_INCREMENT  PRIMARY KEY,
 name VARCHAR(250) NOT NULL,
 code VARCHAR(250) NOT NULL,
 price FLOAT(10) NOT NULL
);

INSERT INTO item (name,code,price) VALUES ('Gaseosa Pepsi', 'PEPSI-CHICA' , 75);
INSERT INTO item (name,code,price) VALUES ('Chicles Bazoka', 'CHICK-BAZK' , 5);
INSERT INTO item (name,code,price) VALUES ('Papas Medianas', 'PAPAS-MED' , 4);


/* ---------------------------------------------------------- */



INSERT INTO stock (itemId,machineId,stock) VALUES (1,1,50);
INSERT INTO stock (itemId,machineId,stock) VALUES (1,2,50);
