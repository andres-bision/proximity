package com.proximity.dto;
import lombok.Data;

@Data
public class MoneyUnitDTO {

	private Integer id;
	private Float value;
	private Integer type;

}
