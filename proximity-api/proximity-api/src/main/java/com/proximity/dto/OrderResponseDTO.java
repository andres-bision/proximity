package com.proximity.dto;
import java.util.List;

import com.proximity.model.PaymentUnits;

import lombok.Data;

@Data
public class OrderResponseDTO {

	private List<PaymentUnits> change;
	private String paymentMethod;
	private String bill;

}
