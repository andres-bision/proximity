package com.proximity.dto;

import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

import lombok.Data;

@Data
public class MachineDTO {
	private Integer id;

	@NotNull
	@Size(min = 0, max = 250)
	private String name;

	@NotNull
	@Size(min = 1, max = 250)
	private String model;

	

	
}
