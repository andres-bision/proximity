package com.proximity.exception;

public class InvalidPaymentException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * To handle a get for an invalid Product
	 */
	public InvalidPaymentException() {
		super("El producto seleccionado es invalido o fue removido.");
	}

}
