package com.proximity.exception;

public class InvalidMachineException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * To handle a get for an invalid Product
	 */
	public InvalidMachineException() {
		super("El producto seleccionado es invalido o fue removido.");
	}

}
