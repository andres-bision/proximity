package com.proximity.exception;

public class InvalidPaymentUnitException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * To handle a get for an invalid Product
	 */
	public InvalidPaymentUnitException() {
		super("Se ha cargado un pago invalido");
	}

}
