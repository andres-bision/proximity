package com.proximity.machine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.proximity.dto.OrderResponseDTO;
import com.proximity.exception.InvalidPaymentUnitException;
import com.proximity.exception.UnsufficientMoneyException;
import com.proximity.model.PaymentUnits;

public class MachineXYZ1 implements IMachine {

	protected static List<PaymentUnits> supported = Arrays.asList(
			PaymentUnits.COIN_01_CENT, 
			PaymentUnits.COIN_05_CENTS, 
			PaymentUnits.COIN_10_CENTS,
			PaymentUnits.COIN_25_CENTS,
			PaymentUnits.COIN_50_CENTS,
			PaymentUnits.BILL_1_DOLAR,
			PaymentUnits.BILL_2_DOLAR);

	private static List<PaymentUnits> paymentsForChange = Arrays.asList(
			PaymentUnits.COIN_01_CENT, 
			PaymentUnits.COIN_05_CENTS, 
			PaymentUnits.COIN_10_CENTS,
			PaymentUnits.COIN_25_CENTS,
			PaymentUnits.COIN_50_CENTS);
	
	public OrderResponseDTO processPayment(List<PaymentUnits> payment, int totalToPay) {

		boolean notAllowed = payment.stream().anyMatch(item -> !supported.contains(item));

		if (notAllowed)
			throw new InvalidPaymentUnitException();

		int totalChargeInMachine = 0;
		for (PaymentUnits paymentUnits : payment) {
			totalChargeInMachine += paymentUnits.getCentsValue();
		}

		if (totalChargeInMachine < totalToPay)
			throw new UnsufficientMoneyException(totalToPay, totalChargeInMachine);

		int diffToReturn= totalChargeInMachine -totalToPay;
		
		List<PaymentUnits> changeToReturn = new ArrayList<PaymentUnits>();
		
		if (diffToReturn > 0) {
			changeToReturn = getChange(totalChargeInMachine -totalToPay );
		}

		OrderResponseDTO a = new OrderResponseDTO();
		a.setChange(changeToReturn);
		a.setPaymentMethod("CASH");
		return a;
	}
	
	public void chechTotal(List<PaymentUnits> payment, int totalToPay) {

		int totalChargeInMachine = 0;
		for (PaymentUnits paymentUnits : payment) {
			totalChargeInMachine += paymentUnits.getCentsValue();
		}

		if (totalChargeInMachine < totalToPay)
			throw new UnsufficientMoneyException(totalToPay, totalChargeInMachine);



	}
	
	public List<PaymentUnits> getChange( int ammoutToReturn  ){

		List<PaymentUnits> change = new ArrayList<PaymentUnits>();
		
		while (ammoutToReturn > 0) {
			Optional<PaymentUnits> aaa = getBiggerThan(ammoutToReturn, paymentsForChange);
			change.add(aaa.get());
			ammoutToReturn -= aaa.get().getCentsValue();
		}
		
		return change;
	}
	
	public Optional<PaymentUnits> getBiggerThan( int value, List<PaymentUnits> pay  ){

		return pay.stream().filter( p -> p.getCentsValue() <= value ).sorted( new Comparator<PaymentUnits>() {

			@Override
			public int compare(PaymentUnits arg0, PaymentUnits arg1) {
				return arg1.getCentsValue().compareTo(arg0.getCentsValue());
			}
		}).findFirst();
		
	}
	

}
