package com.proximity.machine;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.proximity.dto.OrderResponseDTO;
import com.proximity.exception.InvalidPaymentUnitException;
import com.proximity.model.PaymentUnits;

public class MachineXYZ2 extends MachineXYZ1 implements IMachine  {
		
	@Override
	public OrderResponseDTO processPayment(List<PaymentUnits> payment, int totalToPay) {

		Optional<PaymentUnits> notCashPayment = payment.stream().filter(item -> !supported.contains(item))
				.findAny();

		boolean onlyInCashAllowed = notCashPayment.isEmpty();
		boolean paymentOnlyWithCreditCard = !payment.isEmpty() && payment.size() == 1
				&& payment.get(0).equals(PaymentUnits.CREDIT_CARD);

		if (!(onlyInCashAllowed || paymentOnlyWithCreditCard))
			throw new InvalidPaymentUnitException();

		if (onlyInCashAllowed) {
			chechTotal(payment, totalToPay);
		}
		
		if(paymentOnlyWithCreditCard)
			System.out.println("Llamo a la API de tarjetas");
		
		
		OrderResponseDTO a = new OrderResponseDTO();
		a.setPaymentMethod(onlyInCashAllowed ? "CASH" : "CREDIT_CARD");

		return a;
		
	}

}
