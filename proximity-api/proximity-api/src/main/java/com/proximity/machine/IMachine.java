package com.proximity.machine;

import java.util.List;

import com.proximity.dto.OrderResponseDTO;
import com.proximity.model.PaymentUnits;

public interface IMachine {

	public OrderResponseDTO processPayment(List<PaymentUnits> payment, int totalToCharge);
	
}
