package com.proximity.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.proximity.dto.ItemDTO;
import com.proximity.dto.MachineDTO;
import com.proximity.dto.OrderDTO;
import com.proximity.dto.OrderResponseDTO;
import com.proximity.dto.StockDTO;
import com.proximity.model.TransactionModel;
import com.proximity.service.IItemService;
import com.proximity.service.IMachineService;
import com.proximity.service.ITransactionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/machine")
public class MachineController {

	protected final static int DEFAULT_PAGE_SIZE = 8;

	@Autowired
	private IMachineService itemService;

	@Autowired
	private ITransactionService transacionService;
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	@ResponseBody
	public MachineDTO createItem(@Valid @RequestBody MachineDTO req) {
		return itemService.createItem(req);
	}

	@PutMapping("/{id}")
	@ResponseBody
	public MachineDTO updateItem(@PathVariable Integer id, @RequestBody @Valid MachineDTO req) {
		MachineDTO response = itemService.updateItem(id, req);
		return response;
	}

	@GetMapping("/{id}")
	@ResponseBody
	public MachineDTO getItem(@PathVariable Integer id) {
		MachineDTO response = itemService.getItem(id);
		return response;
	}

	@DeleteMapping("/{id}")
	@ResponseBody
	public void deleteProduct(@PathVariable Integer id) {
		itemService.deleteItem(id);
	}

	@GetMapping
	public Page<MachineDTO> getItem(@PageableDefault(size = DEFAULT_PAGE_SIZE) Pageable pageable) {
		Page<MachineDTO> response = itemService.getAllItem(pageable);
		return response;
	}

	@PostMapping("/{id}/buy")
	@ResponseBody
	public OrderResponseDTO buyItem(@PathVariable Integer id, @Valid @RequestBody OrderDTO order) {
		return itemService.buyProduct(id, order);
	}

	@PostMapping("/{id}/stock")
	@ResponseBody
	public MachineDTO changeItem(@PathVariable Integer id, @Valid @RequestBody StockDTO stock) {
		return itemService.changeStock(id, stock);
	}

	
	@GetMapping("/{id}/transactions")
	@ResponseBody
	public List<TransactionModel> getTransactions(@PathVariable Integer id) {
		List<TransactionModel> response = transacionService.getTransactions(id);
		return response;
	}
	
}
