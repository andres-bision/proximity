package com.proximity.service;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.proximity.dto.MachineDTO;
import com.proximity.dto.OrderDTO;
import com.proximity.dto.OrderResponseDTO;
import com.proximity.dto.StockDTO;

public interface IMachineService {

	/**
	 * Create a product and return its DTO.
	 * @param req
	 * @return
	 */
	MachineDTO createItem(MachineDTO req);

	/**
	 * Returns all products paginated
	 * @param pageable
	 * @return
	 */
	Page<MachineDTO> getAllItem(Pageable pageable);

	/**
	 * Return the DTO of a specific product
	 * @param id
	 * @return
	 */
	MachineDTO getItem(Integer id);

	/**
	 * Makes a sell checking at first the product exist and the ammount
	 * in the order is smaller than stock;
	 * @param id
	 * @param order
	 * @return
	 */
	OrderResponseDTO buyProduct(Integer id, OrderDTO order);

	/**
	 * Changes the stock for an specific product.
	 * @param id
	 * @param stock
	 * @return
	 */
	MachineDTO changeStock(Integer id, StockDTO stock);

	/**
	 * Remove a product.
	 * @param id
	 */
	void deleteItem(Integer id);

	/**
	 * Update product and return its DTO.
	 * @param id
	 * @param req
	 * @return
	 */
	MachineDTO updateItem(Integer id, @Valid MachineDTO req);

}
