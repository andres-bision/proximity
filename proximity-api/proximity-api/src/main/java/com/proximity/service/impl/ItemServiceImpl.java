package com.proximity.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.proximity.dto.ItemDTO;
import com.proximity.dto.OrderDTO;
import com.proximity.dto.StockDTO;
import com.proximity.exception.InvalidItemException;
import com.proximity.exception.OutOfStockProductException;
import com.proximity.model.ItemModel;
import com.proximity.model.repository.ItemRepository;
import com.proximity.service.IItemService;

@Service
public class ItemServiceImpl implements IItemService {

	private final static Logger LOGGER = Logger.getLogger(ItemServiceImpl.class.getName());

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public ItemDTO createItem(ItemDTO req) {
		ItemModel newProduct = modelMapper.map(req, ItemModel.class);
		ItemModel saveResult = itemRepository.save(newProduct);
		ItemDTO response = modelMapper.map(saveResult, ItemDTO.class);
		return response;
	}

	@Override
	public ItemDTO updateItem(Integer id, @Valid ItemDTO req) {

		Optional<ItemModel> optionalProd = itemRepository.findById(id);
		if (!optionalProd.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Product ID:" + id);
			throw new InvalidItemException();
		}
		
		ItemModel prod = optionalProd.get();
		prod.setName(req.getName());
		prod.setCode(req.getCode());
		
		ItemModel saveResult = itemRepository.save(prod);
		return modelMapper.map(saveResult, ItemDTO.class);
		
		
	}
	
	@Override
	public Page<ItemDTO> getAllItem(Pageable pageable) {
		Page<ItemModel> reportList = itemRepository.findAll(pageable);
		List<ItemDTO> aux = new ArrayList<ItemDTO>();
		for (ItemModel prodModel : reportList.getContent()) {
			aux.add(modelMapper.map(prodModel, ItemDTO.class));
		}
		Page<ItemDTO> response = new PageImpl<ItemDTO>(aux, pageable, reportList.getTotalElements());
		return response;
	}

	@Override
	public ItemDTO getItem(Integer id) {
		Optional<ItemModel> optionalProd = itemRepository.findById(id);
		if (!optionalProd.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Product ID:" + id);
			throw new InvalidItemException();
		}
		return modelMapper.map(optionalProd.get(), ItemDTO.class);
	}

	/**
	 * This method is synchronized to avoid inconsistencies in parallel calls
	 * between the stock checking and the stock removal
	 */
	@Override
	public synchronized ItemDTO buyProduct(Integer productId, OrderDTO order) {
/*
		Optional<ItemModel> optionalProd = itemRepository.findById(productId);
		if (!optionalProd.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Product ID:" + productId);
			throw new InvalidProductException();
		}
		ItemModel prod = optionalProd.get();

		if (prod.getStock() < order.getAmmount() || order.getAmmount() < 0) {
			LOGGER.log(Level.SEVERE, "OutOfStock ID:" + productId + ". Current Stock:" + prod.getStock() + ". Order Stock"
					+ order.getAmmount());
			throw new OutOfStockProductException();
		}

		prod.setStock(prod.getStock() - order.getAmmount());
		ItemModel saveResult = itemRepository.save(prod);
		return modelMapper.map(saveResult, ItemDTO.class);

	*/
		return null;

	}

	@Override
	public synchronized ItemDTO changeStock(Integer id, StockDTO stock) {
/*
		Optional<ItemModel> optionalProd = itemRepository.findById(id);
		if (!optionalProd.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Product ID:" + id);
			throw new InvalidProductException();
		}

		ItemModel product = optionalProd.get();
		product.setStock(stock.getAmmount());
		ItemModel saveResult = itemRepository.save(product);
		return modelMapper.map(saveResult, ItemDTO.class);
		
		*/
		return null;
	}

	@Override
	public void deleteItem(Integer id) {
		itemRepository.deleteById(id);
	}



}
