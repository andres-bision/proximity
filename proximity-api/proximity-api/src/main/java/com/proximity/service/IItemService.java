package com.proximity.service;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.proximity.dto.ItemDTO;
import com.proximity.dto.OrderDTO;
import com.proximity.dto.StockDTO;

public interface IItemService {

	/**
	 * Create a product and return its DTO.
	 * @param req
	 * @return
	 */
	ItemDTO createItem(ItemDTO req);

	/**
	 * Returns all products paginated
	 * @param pageable
	 * @return
	 */
	Page<ItemDTO> getAllItem(Pageable pageable);

	/**
	 * Return the DTO of a specific product
	 * @param id
	 * @return
	 */
	ItemDTO getItem(Integer id);

	/**
	 * Makes a sell checking at first the product exist and the ammount
	 * in the order is smaller than stock;
	 * @param id
	 * @param order
	 * @return
	 */
	ItemDTO buyProduct(Integer id, OrderDTO order);

	/**
	 * Changes the stock for an specific product.
	 * @param id
	 * @param stock
	 * @return
	 */
	ItemDTO changeStock(Integer id, StockDTO stock);

	/**
	 * Remove a product.
	 * @param id
	 */
	void deleteItem(Integer id);

	/**
	 * Update product and return its DTO.
	 * @param id
	 * @param req
	 * @return
	 */
	ItemDTO updateItem(Integer id, @Valid ItemDTO req);

}
