package com.proximity.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.proximity.dto.ItemDTO;
import com.proximity.dto.MachineDTO;
import com.proximity.dto.OrderDTO;
import com.proximity.dto.StockDTO;
import com.proximity.model.TransactionModel;

public interface ITransactionService {



	List<TransactionModel> getTransactions(Integer id);

}
