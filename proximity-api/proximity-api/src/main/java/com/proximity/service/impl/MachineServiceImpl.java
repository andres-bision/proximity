package com.proximity.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.proximity.dto.ItemDTO;
import com.proximity.dto.MachineDTO;
import com.proximity.dto.OrderDTO;
import com.proximity.dto.OrderResponseDTO;
import com.proximity.dto.StockDTO;
import com.proximity.exception.InvalidItemException;
import com.proximity.exception.InvalidMachineException;
import com.proximity.exception.InvalidPaymentException;
import com.proximity.exception.OutOfStockProductException;
import com.proximity.machine.IMachine;
import com.proximity.machine.MachineFactory;
import com.proximity.model.ItemModel;
import com.proximity.model.MachineModel;
import com.proximity.model.PaymentUnits;
import com.proximity.model.StockModel;
import com.proximity.model.TransactionModel;
import com.proximity.model.repository.ItemRepository;
import com.proximity.model.repository.MachineRepository;
import com.proximity.model.repository.StockRepository;
import com.proximity.model.repository.TransactionRepository;
import com.proximity.service.IMachineService;

@Service
public class MachineServiceImpl implements IMachineService {

	private final static Logger LOGGER = Logger.getLogger(MachineServiceImpl.class.getName());


	
	@Autowired
	private MachineRepository machineRepository;

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private StockRepository stockRepository;

	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public MachineDTO createItem(MachineDTO req) {
		MachineModel newProduct = modelMapper.map(req, MachineModel.class);
		MachineModel saveResult = machineRepository.save(newProduct);
		MachineDTO response = modelMapper.map(saveResult, MachineDTO.class);
		return response;
	}

	@Override
	public MachineDTO updateItem(Integer id, @Valid MachineDTO req) {

		Optional<MachineModel> optionalProd = machineRepository.findById(id);
		if (!optionalProd.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Product ID:" + id);
			throw new InvalidItemException();
		}

		MachineModel prod = optionalProd.get();

		MachineModel saveResult = machineRepository.save(prod);
		return modelMapper.map(saveResult, MachineDTO.class);

	}

	@Override
	public Page<MachineDTO> getAllItem(Pageable pageable) {
		Page<MachineModel> reportList = machineRepository.findAll(pageable);
		List<MachineDTO> aux = new ArrayList<MachineDTO>();
		for (MachineModel prodModel : reportList.getContent()) {
			aux.add(modelMapper.map(prodModel, MachineDTO.class));
		}
		Page<MachineDTO> response = new PageImpl<MachineDTO>(aux, pageable, reportList.getTotalElements());
		return response;
	}

	@Override
	public MachineDTO getItem(Integer id) {
		Optional<MachineModel> optionalProd = machineRepository.findById(id);
		if (!optionalProd.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Product ID:" + id);
			throw new InvalidItemException();
		}
		return modelMapper.map(optionalProd.get(), MachineDTO.class);
	}

	/**
	 * This method is synchronized to avoid inconsistencies in parallel calls
	 * between the stock checking and the stock removal
	 * @return 
	 */
	@Override
	public synchronized OrderResponseDTO buyProduct(Integer productId, OrderDTO order) {

		Optional<MachineModel> optionalProd = machineRepository.findById(productId);
		if (!optionalProd.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Machine ID:" + productId);
			throw new InvalidMachineException();
		}

		Optional<ItemModel> optionalItem = itemRepository.findById(order.getItemId());
		if (!optionalItem.isPresent()) {
			LOGGER.log(Level.SEVERE, "Invalid Item ID:" + order.getItemId());
			throw new InvalidItemException();
		}

		StockModel available = stockRepository.findByMachineIdAndItemId(productId, order.getItemId());

		if (available.getStock() < order.getQuantity() || order.getQuantity() < 0) {
			LOGGER.log(Level.SEVERE, "OutOfStock ID:" + productId + ". Current Stock:" + available.getStock()
					+ ". Order Stock" + order.getQuantity());
			throw new OutOfStockProductException();
		}

		IMachine machine = MachineFactory.create(optionalProd.get().getModel());
			
		
		int sellAmmount = order.getQuantity() * optionalItem.get().getPrice();
		
		OrderResponseDTO aa = machine.processPayment(order.getPayment(), sellAmmount );	
		available.setStock(available.getStock() - order.getQuantity());
		
		
		
		
		StockModel saveResult = stockRepository.save(available);

		
		
		TransactionModel entity = new TransactionModel();
		entity.setQuantity(order.getQuantity());
		entity.setItemId(order.getItemId());
		entity.setMethodPayment(aa.getPaymentMethod());
		entity.setTimestamp(System.currentTimeMillis());
		entity.setTotal(sellAmmount);
		
		
		transactionRepository.save(entity);
		
		return aa;
		
	}


	
	
	@Override
	public synchronized MachineDTO changeStock(Integer id, StockDTO stock) {
		/*
		 * Optional<MachineModel> optionalProd = itemRepository.findById(id); if
		 * (!optionalProd.isPresent()) { LOGGER.log(Level.SEVERE, "Invalid Product ID:"
		 * + id); throw new InvalidProductException(); }
		 * 
		 * MachineModel product = optionalProd.get();
		 * product.setStock(stock.getAmmount()); MachineModel saveResult =
		 * itemRepository.save(product); return modelMapper.map(saveResult,
		 * MachineDTO.class);
		 * 
		 */
		return null;
	}

	@Override
	public void deleteItem(Integer id) {
		machineRepository.deleteById(id);
	}

}
