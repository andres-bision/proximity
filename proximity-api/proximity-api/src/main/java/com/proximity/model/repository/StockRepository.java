package com.proximity.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.proximity.model.StockModel;



@Repository
public interface StockRepository extends PagingAndSortingRepository<StockModel, Integer> {

	StockModel findByMachineIdAndItemId(Integer machineId, Integer itemId);
	
}
