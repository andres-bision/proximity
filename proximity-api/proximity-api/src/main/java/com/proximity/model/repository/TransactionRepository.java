package com.proximity.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.proximity.model.StockModel;
import com.proximity.model.TransactionModel;



@Repository
public interface TransactionRepository extends PagingAndSortingRepository<TransactionModel, Integer> {

	
}
