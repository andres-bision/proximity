package com.proximity.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "stock")
@IdClass(StockId.class)
public class StockModel {

	@Id
	@Column(name = "itemid")
	private Integer itemId;
	@Id
	@Column(name = "machineid")
	private Integer machineId;
	
	private Integer stock;

}

@Data
class StockId implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer itemId;
	private Integer machineId;

}