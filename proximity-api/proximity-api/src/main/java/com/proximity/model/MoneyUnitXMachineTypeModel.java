package com.proximity.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "moneyUnitxmachinetype")
@IdClass(KeyId.class)
public class MoneyUnitXMachineTypeModel {

	@Id
	@Column(name = "moneyunitid")
	private Integer moneyUnitId;
	@Id
	@Column(name = "machineid")
	private Integer machineId;
	
}

@Data
class KeyId implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer moneyUnitId;
	private Integer machineId;

}