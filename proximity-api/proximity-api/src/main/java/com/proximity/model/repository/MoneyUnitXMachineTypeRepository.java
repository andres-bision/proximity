package com.proximity.model.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.proximity.model.MoneyUnitXMachineTypeModel;
import com.proximity.model.StockModel;



@Repository
public interface MoneyUnitXMachineTypeRepository extends PagingAndSortingRepository<MoneyUnitXMachineTypeModel, Integer> {

	
}
